package Homework21;


import Homework21.repositories.UsersRepository;
import Homework21.repositories.UsersRepositoryFilesImpl;
import Homework21.services.UsersServiceImpl;
import Homework21.util.IdGenerator;
import Homework21.util.IdGeneratorFilesImpl;
import Homework21.validators.EmailFormatValidator;
import Homework21.validators.EmailValidator;
import Homework21.validators.PasswordLengthValidator;
import Homework21.validators.PasswordValidator;

public class Main {

    public static void main(String[] args) {
//        UsersRepository usersRepository = new UsersRepositoryFakeImpl();
        IdGenerator idGenerator = new IdGeneratorFilesImpl("users_id_sequence.txt");
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt", idGenerator);
        EmailValidator emailValidator = new EmailFormatValidator();
        PasswordValidator passwordValidator = new PasswordLengthValidator();
        UsersServiceImpl usersService = new UsersServiceImpl(usersRepository, emailValidator, passwordValidator);
        usersService.signUp("shami@gmail.com", "qwerty002");

    }
}
