package Homework21.services;

import Homework21.services.exceptions.AuthenticationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {

    /**
     * Регистрирует пользователя в системе
     * @param email Email пользователя
     * @param password пароль пользователя
     */
    void signUp(String email, String password);

    /**
     * Проверяет, есть ли пользователь в базе данных с такими учетными данными
     * @param email Email-пользователя
     * @param password Пароль пользователя
     * @throws AuthenticationException в случае, если email/пароль неверные
     */
    void signIn(String email, String password) throws AuthenticationException;
}
