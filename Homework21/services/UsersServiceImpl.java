package Homework21.services;


import Homework21.models.User;
import Homework21.services.exceptions.UnuniqueEmailException;
import Homework21.repositories.UsersRepository;
import Homework21.services.exceptions.AuthenticationException;
import Homework21.validators.EmailValidator;
import Homework21.validators.PasswordValidator;

import java.util.Optional;


/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public UsersServiceImpl(UsersRepository usersRepository,
                            EmailValidator emailValidator,
                            PasswordValidator passwordValidator) {
        this.usersRepository = usersRepository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    public void signUp(String email, String password) throws UnuniqueEmailException {
        // сделали валидацию данных
        emailValidator.validate(email);
        passwordValidator.validate(password);
        if (usersRepository.findByEmail(email).isPresent()) {
            throw new UnuniqueEmailException();
        } else {
            User user = new User(email, password);
            // сохранили пользователя
            usersRepository.save(user);
        }
    }


    @Override
    public void signIn(String email, String password) throws AuthenticationException {
        // находим пользователя по его email-у
        Optional<User> userOptional = usersRepository.findByEmail(email);
        // если мне пришел конкретный пользователь
        if (userOptional.isPresent()) {
            // смотрим его пароль
            User user = userOptional.get();
            if (!user.getPassword().equals(password)) {
                throw new AuthenticationException();
            }
            // если все ок - просто останавливаем работу процедуры
            return;
        }
        // если пользователь не пришел
        throw new AuthenticationException();
    }
}
