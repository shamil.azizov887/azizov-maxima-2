package Homework21.util;

import java.io.*;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IdGeneratorFilesImpl implements IdGenerator {

    private final String fileName;

    public IdGeneratorFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Long generate() {
        // получаем последний сгенерированный id из файла
        Long lastGeneratedId = getLastGeneratedId();
        // увеличили на единицу
        Long newId = lastGeneratedId + 1;
        // записали обратно в файл
        writeLastGeneratedId(newId);
        // вернули в качестве результата
        return newId;
    }

    private void writeLastGeneratedId(Long id) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(id.toString());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Long getLastGeneratedId() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            // прочитали как строку последний сгенерированный id
            String lastGeneratedId = reader.readLine();
            // вернули его как Long
            return Long.parseLong(lastGeneratedId);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
