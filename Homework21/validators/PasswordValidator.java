package Homework21.validators;

import Homework21.validators.exceptions.PasswordValidationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PasswordValidator {
    void validate(String password) throws PasswordValidationException;
}
