package Homework21.validators;


import Homework21.validators.exceptions.EmailValidationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EmailFormatValidator implements EmailValidator {




    @Override
    public void validate(String email) throws EmailValidationException {

        if (!email.contains("@")) {
            throw new EmailValidationException();

        }
    }



}

