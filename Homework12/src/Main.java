/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(3, 3, 3, 4);
        Square square = new Square(3, 3, 3);
        Ellipse ellipse = new Ellipse(3, 3, 5, 6);
        Circle circle = new Circle(3, 3, 8);
        System.out.println("P прямоугольника " + rectangle.getPerimeter() + " XY:  " + rectangle.CenterK());
        System.out.println("P квадрата " + square.getPerimeter() + " XY: " + square.CenterK());
        System.out.println("P эллипса " + ellipse.getPerimeter() + " XY: " + ellipse.CenterK());
        System.out.println("P круга " + circle.getPerimeter() + " XY: " + circle.CenterK());

        rectangle.move(66, 77);
        square.move(66, 77);
        ellipse.move(66, 77);
        circle.move(66, 77);

        System.out.println("Новые XY прямоугольника " + rectangle.CenterK());
        System.out.println("Новые XY квадрата " + square.CenterK());
        System.out.println("Новые XY эллипса " + ellipse.CenterK());
        System.out.println("Новые XY круга " + circle.CenterK());

    }
}
