/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Circle extends Ellipse{
    private double Rad1;

    public Circle(int x, int y, int Rad1 ) {
        super(x, y, Rad1, Rad1);
        this.Rad1 = Rad1;
    }


    public double getPerimeter() {
        return (float) (Math.PI * Math.sqrt(Rad1));
    }
}


