/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// овал
public class Ellipse extends Figure {
    private double Rad1;
    private double Rad2;

    public Ellipse(double x, double y, double Rad1, double Rad2) {
        super(x, y);

        this.Rad1 = Rad1;
        this.Rad2 = Rad2;
    }

    public double getPerimeter() {
        return 4 * (Math.PI * Rad1 * Rad2 + Math.pow((Rad1 - Rad2), 2)) / (Rad1 + Rad2);
    }
}




