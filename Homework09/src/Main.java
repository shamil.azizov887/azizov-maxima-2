public class Main {

    public static void main(String[] args) {
        Driver driver = new Driver("Семен", 10);

        Bus bus = new Bus(99, "Нефаз", 5);
        System.out.println(bus.getState());
        bus.setState(0);
        System.out.println(bus.getState());
        driver.drive(bus);

        Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Айрат");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger4.goToBus(bus);
        passenger5.goToBus(bus);
        passenger6.goToBus(bus);

        bus.setDriver(driver);



    }
}
