
public class Driver {
    private String name;
    private int experience;
    private Bus bus;


    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }


    //Метод drive (чтобы завести автобус автобуса)
    public void drive(Bus bus) {
        //Проверка: заведен ли автобус?
        if (bus.getState()==0){  // Если не заведен

            bus.setState(1);



            System.out.println("Водитель " + name + " завел автобус ");

            for (int i = 0; i < bus.getPassengers().length; i++) {
                bus.getPassengers()[i].sayName();
            }

            System.out.println("Меня зовут " + name);


       }
        else { // Если заведен

            System.out.println("Водитель " + name + " уже завел автобус ");
        }
   }





}
